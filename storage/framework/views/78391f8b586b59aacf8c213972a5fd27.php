<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Home | E-Shopper</title>
    <link href="<?php echo e(asset('member/css/bootstrap.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('member/css/font-awesome.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('member/css/prettyPhoto.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('member/css/price-range.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('member/css/animate.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('member/css/main.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('member/css/responsive.css')); ?>" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="<?php echo e(asset('member/js/html5shiv.js')); ?>"></script>
    <script src="<?php echo e(asset('member/js/respond.min.js')); ?>"></script>
    <![endif]-->
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
    <?php echo $__env->yieldContent('js'); ?>
</head>
<!--/head-->

<body>
    <?php echo $__env->yieldContent('main'); ?>
    <script src="<?php echo e(asset('member/js/jquery.js')); ?>"></script>
    <script src="<?php echo e(asset('member/js/bootstrap.min.js')); ?>"></script>
    <script src="<?php echo e(asset('member/js/jquery.scrollUp.min.js')); ?>"></script>
    <script src="<?php echo e(asset('member/js/price-range.js')); ?>"></script>
    <script src="<?php echo e(asset('member/js/jquery.prettyPhoto.js')); ?>"></script>
    <script src="<?php echo e(asset('member/js/main.js')); ?>"></script>
</body>

</html>
<?php /**PATH D:\xampp\htdocs\example-laravel\resources\views/layout/member/mainMail.blade.php ENDPATH**/ ?>