<?php $__currentLoopData = $cmt; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <?php if($item->level == 0): ?>
        <li class="media" style="margin-top: 6%">
            <a class="pull-left" href="#">
                <img style="width:60px; height:60px" class="media-object"
                    src="<?php echo e(asset('upload/user/avatar/' . $item->avatar)); ?>" alt="">
            </a>
            <div class="media-body">
                <ul class="sinlge-post-meta">
                    <li><i class="fa fa-user"></i><?php echo e($item->name); ?></li>
                    <li><i class="fa fa-clock-o"></i> <?php echo e($item->created_at); ?></li>
                    <li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
                </ul>
                <p><?php echo e($item->content); ?></p>
                <input type="hidden" value="<?php echo e($item->id); ?>">
                <a class="button_rl btn btn-primary " id=""><i class="fa fa-reply"></i>Replay</a>
            </div>
        </li>
        <?php $__currentLoopData = $cmt; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php if($value->level != 0 && $value->level == $item->id): ?>
                <li class="media second-media" style="margin-top: -6%">
                    <img style="width:60px; height:60px" class="media-object"
                        src="<?php echo e(asset('upload/user/avatar/' . $value->avatar)); ?>" alt="">
                    <div class="media-body">
                        <ul class="sinlge-post-meta">
                            <li><i class="fa fa-user"></i><?php echo e($value->name); ?></li>
                            <li><i class="fa fa-clock-o"></i> <?php echo e($value->created_at); ?></li>
                            <li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
                        </ul>
                        <p><?php echo e($value->content); ?>

                        </p>

                    </div>
                </li>
            <?php endif; ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <li class="body_rl body_rl_<?php echo e($item->id); ?> " style="display: none">
            <div class="text-area">
                <div class="blank-arrow">
                    <label>
                        <?php if(Auth::check()): ?>
                            <?php echo e(Auth::user()->name); ?>

                        <?php endif; ?>
                    </label>
                </div>
                <textarea name="message" class="message" rows="1"></textarea>
                <input type="hidden" value="<?php echo e($item->id); ?>" class="id-cmt" name="id_cmt">
                <button type="submit" class="btn btn-primary rl-comment" id="">post
                    comment</button>
            </div>
        </li>
    <?php endif; ?>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php /**PATH D:\xampp\htdocs\example-laravel\resources\views/member/bodyComment.blade.php ENDPATH**/ ?>