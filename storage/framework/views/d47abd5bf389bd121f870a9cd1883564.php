
<?php $__env->startSection('main'); ?>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <?php echo $__env->make('layout.member.leftSlide', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                </div>
                <div class="col-sm-9">
                    <div class="blog-post-area">
                        <h2 class="title text-center">Latest From our Blog</h2>
                        <?php if($data): ?>
                            <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="single-blog-post">
                                    <h3><?php echo e($item->title); ?></h3>
                                    <div class="post-meta">
                                        <ul>
                                            <li><i class="fa fa-user"></i> Mac Doe</li>
                                            <li><i class="fa fa-clock-o"></i> 1:33 pm</li>
                                            <li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
                                        </ul>
                                        <span>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-half-o"></i>
                                        </span>
                                    </div>
                                    <a href="<?php echo e(url('/member/blogs/' . $item->id)); ?>">
                                        <img src="<?php echo e(asset('upload/user/blogs/' . $item->image)); ?>" alt="">
                                    </a>
                                    <p><?php echo e($item->description); ?></p>
                                    <p><?php echo $item->content; ?></p>
                                    <a class="btn btn-primary" href="">Read More</a>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>


                        <div class="pagination-area">
                            <?php echo e($data->links('pagination::bootstrap-4')); ?>

                            <ul class="pagination">
                                <li><a href="" class="active">1</a></li>
                                <li><a href="">2</a></li>
                                <li><a href="">3</a></li>
                                <li><a href=""><i class="fa fa-angle-double-right"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout.member.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\example-laravel\resources\views/member/blogs.blade.php ENDPATH**/ ?>